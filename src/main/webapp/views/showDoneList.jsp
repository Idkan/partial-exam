<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Done List</title>
</head>
<body>
	<h3>Showing Done List</h3>
	<hr>
	<table>
		<tr>
			<td>ID</td>
			<td>Activity Name</td>
			<td>Status</td>
		</tr>
		<c:forEach items="${listDone}" var="activity">
               <tr>
                   <td>${activity.getToDoId()}</td>
                   <td>${activity.getList()}</td>
                   <td>${activity.getIs_done()}</td>
                </tr>
           </c:forEach>
	</table>
	<br>
	<a href="./index.jsp">Home Page</a>
</body>
</html>