package com.softtek.academy.javaweb.exam.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.exam.beans.ToDo;
import com.softtek.academy.javaweb.exam.dao.ToDoDAO;

/**
 * Servlet implementation class ToDoListController
 */
public class ToDoListController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToDoListController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
				
		List<ToDo> ToDoList = ToDoDAO.getToDoListDAO();
		request.setAttribute("listToDo", ToDoList);
		RequestDispatcher rd = request.getRequestDispatcher("/views/showToDoList.jsp");
		rd.forward(request, response);
	}

}
