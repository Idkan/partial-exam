package com.softtek.academy.javaweb.exam.dao;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import com.softtek.academy.javaweb.exam.beans.ToDo;
import com.softtek.academy.javaweb.exam.services.ToDoService;

public class ToDoDAO {

	public static void insertListDAO(String name) {
		if (!name.isEmpty()) {
			ToDoService.InsertList(name);
		}
	}
	
	public static List<ToDo> getToDoListDAO() {
		List<ToDo> toDoList = ToDoService.getAllList();
		List<ToDo> resultToDoList = toDoList.stream()
				 .filter(is_done -> is_done.getIs_done().booleanValue() == false)
				 .collect(Collectors.toList());
		return resultToDoList;
	}
	
	public static List<ToDo> getDoneListDAO() {
		List<ToDo> doneList = new ArrayList<ToDo>();
		doneList = ToDoService.getAllList();
		List<ToDo> resultDoneList = doneList.stream()
				  .filter(is_done -> is_done.getIs_done().booleanValue() == true)
				  .collect(Collectors.toList());
		return resultDoneList;
	}
	
	public static void updateListDAO(int toDoId) {
		if (toDoId >= 0) {
			ToDoService.UpdateList(toDoId);
		}
	}
}
