package com.softtek.academy.javaweb.exam.connection;

import java.sql.Connection;
import java.sql.DriverManager;

public class MySQLConnection {
	
	private static MySQLConnection connectionInstance;
		
	public static Connection getConnection() {
		
		final String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
		final String DB_URL = "jdbc:mysql://127.0.0.1:3306/sesion5?serverTimezone=UTC#";
		final String USER = "root";
		final String PASS = "1234";
		
		Connection conn = null;
		
		try {
			if (connectionInstance == null) {
				Class.forName(JDBC_DRIVER);
				conn = DriverManager.getConnection(DB_URL, USER,PASS);
			}
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
		return conn;
	}

}
