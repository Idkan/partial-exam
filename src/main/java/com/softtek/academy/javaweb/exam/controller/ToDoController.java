package com.softtek.academy.javaweb.exam.controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.exam.dao.ToDoDAO;

/**
 * Servlet implementation class ToDoController
 */
public class ToDoController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ToDoController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String activityName = request.getParameter("listName");
		ToDoDAO.insertListDAO(activityName);
		
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		String title = "Using POST method to read from data";
		String docType = "<!doctype html public \"-//w3c//dtd html 4.0 " + "transitional//en\">\n";
		
		out.println(docType + 
				"<html>\r\n" + 
				"<head>\r\n" + 
				"<title>" + title + "</title>\r\n" + 
				"</head>\r\n" +
				"<body>\r\n" +  
				"<h3>What do you want to do?</h3>\r\n" +
				"<hr>\r\n" +
				"<a href=\"/Partial-Exam/views/newListForm.jsp\">\r\n" + 
				"    <button>Add New Activity</button>\r\n" + 
				"</a>" +
				"<br>" +
				"		<form action=\"/Partial-Exam/ToDoListControllerPage\" method=\"post\">\r\n" + 
				"			<button type=\"submit\">Go to My To Do List</button>\r\n" + 
				"		</form>" + 
				"		<form action=\"/Partial-Exam/DoneControllerPage\" method=\"get\">\r\n" + 
				"			<button type=\"submit\">Go to My Done List</button>\r\n" + 
				"		</form>"  +
				"</body>\r\n" + 
				"</html>");	
	}

}
