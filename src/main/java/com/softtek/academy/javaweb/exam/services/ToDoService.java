package com.softtek.academy.javaweb.exam.services;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;

import com.softtek.academy.javaweb.exam.beans.ToDo;
import com.softtek.academy.javaweb.exam.connection.MySQLConnection;

public class ToDoService {

public static void InsertList (String nameList) {
		
		try {
			Connection con = MySQLConnection.getConnection();
			con = MySQLConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("INSERT INTO TO_DO_LIST(list) VALUES(?)");
			ps.setString(1, nameList);
			ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e);
		}
	}
	
	public static ArrayList<ToDo> getAllList() {
		
		ArrayList<ToDo> toDoList = new ArrayList<ToDo>();
		
		try {
			Connection con = MySQLConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("SELECT * FROM TO_DO_LIST");
			ResultSet rs = ps.executeQuery();
			while(rs.next()) {
				ToDo activity = new ToDo();
				activity.setToDoId(rs.getInt("ID"));
				activity.setList(rs.getString("LIST"));
				activity.setIs_done(rs.getBoolean("IS_DONE"));
				toDoList.add(activity);
			}

			return toDoList;

		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
			return toDoList;
		}
		
	}
	
	public static void UpdateList (int ToDoId) {
		
		try {
			Connection con = MySQLConnection.getConnection();
			PreparedStatement ps = con.prepareStatement("UPDATE TO_DO_LIST SET IS_DONE = 1 WHERE ID = ?");
			ps.setInt(1, ToDoId);
			ps.executeUpdate();
		} catch (Exception e) {
			// TODO: handle exception
			System.out.println(e.getMessage());
		}
		
	}
}
