package com.softtek.academy.javaweb.exam.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.softtek.academy.javaweb.exam.beans.ToDo;
import com.softtek.academy.javaweb.exam.dao.ToDoDAO;

/**
 * Servlet implementation class DoneController
 */
public class DoneController extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DoneController() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		List<ToDo> DoneList = ToDoDAO.getDoneListDAO();
		request.setAttribute("listDone", DoneList);
		RequestDispatcher rd = request.getRequestDispatcher("/views/showDoneList.jsp");
		rd.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String[] checkedActivities = request.getParameterValues("idIs_Done");
		int[] activitiesId = new int[checkedActivities.length];
		for (int i = 0; i < checkedActivities.length; i++) {
			activitiesId[i] =  Integer.parseInt(checkedActivities[i]);
		}
		for (int j : activitiesId) {
			ToDoDAO.updateListDAO(j);
		}
		List<ToDo> DoneList = ToDoDAO.getDoneListDAO();
		request.setAttribute("listDone", DoneList);
		RequestDispatcher rd = request.getRequestDispatcher("/views/showDoneList.jsp");
		rd.forward(request, response);
	}

}
