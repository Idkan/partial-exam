package com.softtek.academy.javaweb.exam.beans;

public class ToDo {
	
	private int ToDoId;
	private String list;
	private Boolean is_done;
	
	public ToDo() {}
	
	public ToDo(int toDoId, String list, Boolean is_done) {
		super();
		ToDoId = toDoId;
		this.list = list;
		this.is_done = is_done;
	}

	public int getToDoId() {
		return ToDoId;
	}

	public void setToDoId(int toDoId) {
		ToDoId = toDoId;
	}

	public String getList() {
		return list;
	}

	public void setList(String list) {
		this.list = list;
	}

	public Boolean getIs_done() {
		return is_done;
	}

	public void setIs_done(Boolean is_done) {
		this.is_done = is_done;
	}
	
	
}
