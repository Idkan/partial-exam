<%
	// Scriptlet tag is used to perform Java source code
	String contextPath = request.getContextPath();
	String title = "Index Application";
%>

<html>
	<head>
		<title><%= title %></title>
	</head>
	<body>
		<h2>First Partial Exam Java Web</h2>
		<hr>
		<form action="<%= contextPath%>/views/newListForm.jsp">
			<button type="submit">Create New List</button>
		</form>	
		<form action="./ToDoListControllerPage" method="post">
			<button type="submit">Go to My To Do List</button>
		</form>	
		<form action="./DoneControllerPage" method="get">
			<button type="submit">Go to My Done List</button>
		</form>	
	</body>
</html>
