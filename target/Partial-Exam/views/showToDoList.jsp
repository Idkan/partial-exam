<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>To Do List</title>
</head>
<body>
	<h3>Showing To Do List</h3>
	<hr>
	<form action="/Partial-Exam/DoneControllerPage" method="POST" id="checkedForm">
	<table>
		<tr>
			<td>ID</td>
			<td>Activity Name</td>
			<td>Status</td>
		</tr>
		<c:forEach items="${listToDo}" var="activity">
               <tr>
                   <td>${activity.getToDoId()}</td>
                   <td>${activity.getList()}</td>
                   <td><input type="checkbox" name="idIs_Done" value="${activity.getToDoId()}"></td>
                </tr>
           </c:forEach>
	</table>
	<button type="submit" form="checkedForm" value="Submit" >CHANGE</button>
	</form>
	<br>
	<a href="./index.jsp">Home Page</a>
</body>
</html>